<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 27.10.13
 * Time: 01:46
 * To change this template use File | Settings | File Templates.
 */

namespace DevGarden\PHPBench\BenchmarkBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DevGarden\PHPBench\BenchmarkBundle\Modules\BenchmarkSelfRunCoreProcessor;

class BenchmarkCoreRunCommand extends ContainerAwareCommand{

    const COMMAND_NAME = 'benchmark:self';
    const CONTROLLER_NAME_PATTERN = 'php.bench.benchmark.self.core.processor';

    /**
     * @var BenchmarkSelfRunCoreProcessor
     */
    protected $processor;

    /**
     * defines command behaviour
     */
    protected function configure()
    {
        parent::configure();
        $this->setName(self::COMMAND_NAME)
            ->setDescription('executes clean benchmark without any jobs between in 1k steps and n runs')
            ->addArgument('runs', InputArgument::OPTIONAL, 'runs for benchmark')
            ->addArgument('steps', InputArgument::OPTIONAL, 'steps per run');
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        date_default_timezone_set('Europe/Berlin');
        $runs = $input->getArgument('runs');
        $steps = $input->getArgument('steps');
        if ($runs == 0 && $steps == 0) {
            $output->writeln('starting with default values (1000 steps per 1000 runs)');
        }
        $output->writeln(
            sprintf(
                'Started Benchmark Runs Total %s at %s with integrated steps per run %s',
                $runs,
                date('Y-m-d H:i:s'),
                $steps
            )
        );
        $this->processor = $this->getContainer()->get(self::CONTROLLER_NAME_PATTERN);
        $this->processor->run($runs, $steps);
        $output->writeln(
            sprintf(
                'Finished Benchmark Runs Total %s at %s with integrated steps %s',
                $runs,
                date('Y-m-d H:i:s'),
                $steps
            )
        );

        return true;
    }

}