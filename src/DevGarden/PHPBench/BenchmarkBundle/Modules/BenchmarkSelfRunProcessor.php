<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 27.10.13
 * Time: 02:37
 * To change this template use File | Settings | File Templates.
 */

namespace DevGarden\PHPBench\BenchmarkBundle\Modules;


use DevGarden\PHPBench\BenchmarkBundle\Profiles\BenchmarkProfile;

class BenchmarkSelfRunProcessor {

    protected $steps = 1000;

    /**
     * @var int benchmark runs 2 proceed self benchmark with 1000 steps
     */
    protected $runs = 1000;

    /**
     * @var array includes benchmarkProfiles
     */
    protected $profiles = array();

    /**
     * @param int $runs
     * @param int $steps
     */
    public function run($runs = 0, $steps = 0) {
        $this->runs = $runs < 1 ? $this->runs : $runs;
        $this->steps = $steps < 1 ? $this->steps : $steps;
        $this->proceedBenchmarkProcessRuns();
    }

    /**
     * @return int
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * proceed runs by giving run limit
     */
    protected function proceedBenchmarkProcessRuns(){
        for ( $i=1; $i<=$this->runs; $i++) {
            $this->processBenchmarkRun($i);
        }
        //unset($this->profiles);

    }

    /**
     * @param int $run
     * @return \DevGarden\PHPBench\BenchmarkBundle\Profiles\BenchmarkProfile
     */
    protected function processBenchmarkRun($run) {
        $benchmark = new Benchmark('Selftest.'.$run);
        $benchmark->startInstance();
        for ($i=0;$i<=$this->steps;$i++) {
            $benchmark->addStep('Selftest.step.'.$i);
        }
        $benchmark->finishInstance();
        unset($benchmark);
        //$benchmarkProfile = new BenchmarkProfile($benchmark);
        //$benchmarkProfileData = $benchmarkProfile->getProfileData();
        //unset($benchmark,$benchmarkProfile);
        //return $benchmarkProfileData;
    }
}