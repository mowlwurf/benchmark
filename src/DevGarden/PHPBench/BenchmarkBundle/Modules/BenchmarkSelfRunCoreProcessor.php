<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 27.10.13
 * Time: 02:37
 * To change this template use File | Settings | File Templates.
 */

namespace DevGarden\PHPBench\BenchmarkBundle\Modules;

use DevGarden\PHPBench\BenchmarkBundle\Modules\Benchmark;

class BenchmarkSelfRunCoreProcessor {

    protected $steps = 1000;

    /**
     * @var int benchmark runs 2 proceed self benchmark with 1000 steps
     */
    protected $runs = 1000;

    /**
     * @var array includes benchmarkProfiles
     */
    protected $benchmarkProfiles = array();

    /**
     * @var Benchmark
     */
    protected $benchmark;

    /**
     * @param int $runs
     * @param int $steps
     */
    public function run($runs = 0, $steps = 0) {
        $this->runs = $runs = 0 ? $this->runs : $runs;
        $this->steps = $steps = 0 ? $this->steps : $steps;
        $this->proceedBenchmarkProcessRuns();
    }

    /**
     * proceed runs by giving run limit
     */
    protected function proceedBenchmarkProcessRuns(){
        for ( $i=1; $i<=$this->runs; $i++) {
            $this->benchmarkProfiles = $this->processBenchmarkRun($i);
            unset($this->benchmarkProfiles);
        }
    }

    /**
     * @return \DevGarden\PHPBench\BenchmarkBundle\Modules\Benchmark
     */
    protected function processBenchmarkRun() {
        $this->benchmark = new Benchmark();
        $this->benchmark->startInstance();
        for ($i=1;$i<=$this->steps;$i++) {
            $this->benchmark->addStep('Selftest.step.'.$i);
        }
        $this->benchmark->finishInstance();
        return $this->benchmark;
    }
}