<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 26.10.13
 * Time: 04:07
 * To change this template use File | Settings | File Templates.
 */

namespace DevGarden\PHPBench\BenchmarkBundle\Modules;


class Benchmark {

    /**
     * @var string
     */
    private $instanceName = '';

    /**
     *
     * defines if memory got measured too
     *
     * @var bool
     */
    private $extendedDetails = false;

    /**
     * @var array
     */
    private $instanceSteps = array();

    /**
     * @var int
     */
    private $instanceStepCount = 0;

    /**
     * @var int timestamp
     */
    private $instanceTime = 0;

    /**
     * @var int
     */
    private $instanceMemoryUsage = 0;

    /**
     * @var int
     */
    private $instanceSystemLoad = 0;

    /**
     * @param string $name
     * @param bool $exclusive
     */
    public function __construct($name = '', $exclusive = false){
        $this->setInstanceName($name);
        if ($exclusive) {
            $this->extendedDetails = true;
        }
    }

    /**
     * @return int
     */
    public function getInstanceStepCount()
    {
        return $this->instanceStepCount;
    }

    /**
     * @return int
     */
    public function getInstanceTime()
    {
        return $this->instanceTime;
    }

    /**
     * @return string
     */
    public function getInstanceName()
    {
        return $this->instanceName;
    }

    /**
     * @return int
     */
    public function getInstanceSystemLoad()
    {
        return $this->instanceSystemLoad;
    }

    /**
     * @return array
     */
    public function getInstanceSteps()
    {
        return $this->instanceSteps;
    }

    /**
     * @return int
     */
    public function getInstanceMemoryUsage()
    {
        return $this->instanceMemoryUsage;
    }

    /**
     * @param $instanceName
     */
    public function setInstanceName($instanceName){
        $this->instanceName = $instanceName;
    }

    /**
     *
     */
    public function startInstance(){
        $this->instanceTime = microtime(true);
        if ($this->extendedDetails) {
            $this->instanceMemoryUsage = memory_get_peak_usage(true);
            $this->instanceSystemLoad = sys_getloadavg();
        }
    }

    /**
     *
     */
    public function finishInstance(){
        $this->finishStep($this->instanceStepCount);
        $this->instanceTime = round(microtime(true) - $this->instanceTime, 6);
        if ($this->extendedDetails) {
            $this->instanceMemoryUsage = memory_get_peak_usage(true) - $this->instanceMemoryUsage;
            $this->instanceSystemLoad = sys_getloadavg() - $this->instanceSystemLoad;
        }
    }

    /**
     * @param string $stepName
     */
    public function addStep($stepName = ''){
        if ( $this->instanceStepCount != 0 ) {
            $this->finishStep($this->instanceStepCount);
        }
        $this->initStep($stepName);
    }

    /**
     * @param string $stepName
     */
    protected function initStep($stepName){
        $this->instanceStepCount++;
        $this->instanceSteps[$this->instanceStepCount] = array(
            'stepName' => $stepName,
            'stepTime' => microtime(true),
        );
        if ($this->extendedDetails) {
            $this->instanceSteps[$this->instanceStepCount]['stepMemoryUsage'] = memory_get_peak_usage(true);
            $this->instanceSteps[$this->instanceStepCount]['stepSystemLoad'] = sys_getloadavg();
        }
    }

    /**
     * @param $stepNumber
     */
    protected function finishStep($stepNumber){
        $this->instanceSteps[$stepNumber]['stepTime'] = round(microtime(true) - $this->instanceSteps[$stepNumber]['stepTime'], 6);
        if ($this->extendedDetails) {
            $this->instanceSteps[$stepNumber]['stepMemoryUsage'] = memory_get_peak_usage(true) - $this->instanceSteps[$stepNumber]['stepMemoryUsage'];
            $this->instanceSteps[$stepNumber]['stepSystemLoad'] = sys_getloadavg() - $this->instanceSteps[$stepNumber]['stepSystemLoad'];
        }

    }
}