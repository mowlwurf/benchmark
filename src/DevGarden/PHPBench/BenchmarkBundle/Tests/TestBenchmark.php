<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 26.10.13
 * Time: 04:55
 * To change this template use File | Settings | File Templates.
 */

namespace DevGarden\PHPBench\BenchmarkBundle\Tests;

use DevGarden\PHPBench\BenchmarkBundle\Benchmark;

class TestBenchmark extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Benchmark
     */
    protected $objectInstance;

    public function setUp(){
        $this->handleDependencies();
        $this->objectInstance = new Benchmark();
    }

    public function testDummy(){
        $this->assertTrue(true);
    }

    public function testSetInstanceName(){
        $this->objectInstance->setInstancName('test');
        $this->assertAttributeEquals('test','instanceName', $this->objectInstance);
    }
}