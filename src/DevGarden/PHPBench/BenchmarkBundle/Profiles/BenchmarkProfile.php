<?php
/**
 * Created by JetBrains PhpStorm.
 * User: dnaumann
 * Date: 26.10.13
 * Time: 22:26
 * To change this template use File | Settings | File Templates.
 */

namespace DevGarden\PHPBench\BenchmarkBundle\Profiles;

use DevGarden\PHPBench\BenchmarkBundle\Modules\Benchmark;

class BenchmarkProfile {

    /**
     * @var Benchmark
     */
    protected $benchmark;

    /**
     * @var float
     */
    protected $benchmarkInstanceRuntime = 0;

    /**
     * @var float
     */
    protected $benchmarkInstanceMemoryUsage = 0;

    /**
     * @var string name to group couple of benchmarks
     */
    protected $benchmarkInstanceGroup = '';

    /**
     * @var array holds information about single step execution times
     */
    protected $benchmarkInstanceStepDetails = array();

    /**
     * @param Benchmark $benchmark
     * @param bool $detailed
     */
    public function __construct(Benchmark $benchmark, $detailed = false) {
        $this->benchmark = $benchmark;
        return $this->analyse($detailed);
    }

    /**
     * analyse & identify benchmark execution times for whole run and single steps such as there definitions
     *
     * @param $detailed
     * @return array
     */
    protected function analyse($detailed){
        $this->benchmarkInstanceGroup = $this->benchmark->getInstanceName();

        $this->benchmarkInstanceRuntime =
            $this->benchmark->getInstanceTime() -
                $this->benchmark->getInstanceStartTime();

        $this->benchmarkInstanceMemoryUsage =
            $this->benchmark->getInstanceFinishMemoryUsage() -
                $this->benchmark->getInstanceMemoryUsage();

        if ($detailed) {
            $steps = $this->benchmark->getInstanceSteps();
            foreach ($steps as $step) {
                $this->addInstanceStepDetail($step);
            }
            unset($steps, $stepCount);
        }
        unset($this->benchmark);

        $benchmarkProfileData = array(
            'InstanceGroup' => $this->benchmarkInstanceGroup,
            'InstanceRuntime' => $this->benchmarkInstanceRuntime,
            'InstanceMemoryUsage' => $this->benchmarkInstanceMemoryUsage,
            'InstanceStepDetails' => $this->benchmarkInstanceStepDetails
        );
        unset(
            $this->benchmarkInstanceGroup,$this->benchmarkInstanceRuntime,
            $this->benchmarkInstanceMemoryUsage,$this->benchmarkInstanceStepDetails

        );
        return $benchmarkProfileData;
    }

    /**
     * @param array $step
     */
    protected function addInstanceStepDetail(array $step){
        array_push(
            $this->benchmarkInstanceStepDetails,
            array(
                'definition' =>  $step['stepName'],
                'executionTime' =>
                    $step['stepFinishTime'] - $step['stepStartTime'],
                'memoryUsage' =>
                    $step['stepFinishMemoryUsage'] - $step['stepStartMemoryUsage']
            )
        );
    }
}